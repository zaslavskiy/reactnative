import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from "react-native";
import {useDispatch} from "react-redux";
import {delTodoAction} from "../store/actions/todoAction";

const ToDo = ({item}) => {

  const dispatch = useDispatch();

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      onPress={()=> console.log('Pressed',item.id)}
      onLongPress={()=>dispatch(delTodoAction(item.id))}
    >
      <Text style={styles.text}>{item.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create(
  {
    toDoList: {
      padding: 20,
      // borderStyle: 'solid',
      // borderWidth: 1,
      // borderColor: '#3949ab'
      // backgroundColor: '#3949ab',
      // height: 80,
      // flexDirection: 'row',
      // alignItems: 'center',
      // justifyContent: 'space-between',

      // paddingBottom: 15,
    },
    text: {
      margin: 10,
      marginHorizontal: 20,
      fontSize: 30,
      color: '#3949ab'
      // width: '90%',
      // borderStyle: 'solid',
      // borderWidth: 1,
      // borderColor: '#3949ab'
    }
  }
)

export {ToDo};