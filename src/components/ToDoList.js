import React, {useEffect} from 'react';
import {FlatList, StyleSheet} from "react-native";
import {ToDo} from "./ToDo";
import {loadTodoAction} from "../store/actions/todoAction";
import {useDispatch, useSelector} from "react-redux";

const ToDoList = () => {

  const dispatch = useDispatch();

  useEffect(()=>dispatch(loadTodoAction()),[]);

  const todos = useSelector(store => store.todo)

  return (

    <FlatList data={todos}
              keyExtractor={item => item.id}
              renderItem={({item}) => <ToDo item={item} />}
    />
  )
};


const styles = StyleSheet.create(
  {
  }
)

export {ToDoList};