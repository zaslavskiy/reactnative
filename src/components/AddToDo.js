import React, {useState} from 'react';
import {View, TextInput, Button, StyleSheet, Alert} from 'react-native';
import {useDispatch} from "react-redux";
import {addTodoAction} from "../store/actions/todoAction";


const AddToDo = () => {
  const [value, setValue] = useState('');

  const dispatch = useDispatch();

  const pressHandler = () =>{
    if(value.trim()) {
      dispatch(addTodoAction(value));
      setValue('');
    }else Alert.alert('Название не может быть пустой строкой!')
  }

  return (
    <View style={styles.addForm}>
      <TextInput
        value={value}
        placeholder={'Введите дело...'}
        // onChangeText={text=>setValue(text)}
        onChangeText={setValue}
        autoCorrect={false}
        style={styles.input}/>
      <Button onPress={pressHandler} title={'+'}/>
      {/*<Button title={'+'} color={'#3949ab'} style={styles.button}/>*/}
      {/*на кнопке не применимы стили, только цвет*/}
    </View>
  );
};

const styles = StyleSheet.create(
  {
    addForm: {
      padding: 20,
      // backgroundColor: '#3949ab',
      // height: 80,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',

      // paddingBottom: 15,
    },
    input: {
      color: '#3949ab',
      // marginRight: 10,
      padding: 10,
      width: '87%',
      // color: 'white',
      fontSize: 25,
      // fontWeight: 'bold',
      borderStyle: 'solid',
      borderBottomWidth: 3,
      borderBottomColor: '#3949ab'

    },

  }
)

export {AddToDo};