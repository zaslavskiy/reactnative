import React from 'react';
import {View, Text, StyleSheet} from 'react-native'


export const Navbar = (props) => {
  const {title} = props;
  return (
    <View style={styles.navBar}>
      <Text style={styles.text}>{title}</Text>
    </View>
  );
};

const styles = StyleSheet.create(
  {
    navBar:{
      backgroundColor: '#3949ab',
      height: 80,
      alignItems: 'center',
      justifyContent: 'flex-end',
      paddingBottom: 15,


    },
    text:{
      color: 'white',
      fontSize: 23,
      // fontWeight: 'bold',

    }

  }
)

