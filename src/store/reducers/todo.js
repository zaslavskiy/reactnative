import {ADD_TODO, DEL_TODO, LOAD_TODO} from "../actions/todoAction";

const initialState = []

export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_TODO:
      return action.payload
    case ADD_TODO:
      return [...state, action.payload]
    case DEL_TODO:
      return state.filter((item) => item.id !== action.payload)


    default:
      return state
  }
}