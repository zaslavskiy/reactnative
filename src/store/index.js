import {createStore, combineReducers, applyMiddleware} from 'redux'
import {todoReducer} from './reducers/todo'
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";

const rootReducer = combineReducers({
  todo: todoReducer
})

export default createStore(rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk)))