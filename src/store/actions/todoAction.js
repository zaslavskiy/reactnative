import axios from "axios";

export const LOAD_TODO = 'LOAD_TODO'
export const ADD_TODO = 'ADD_TODO'
export const DEL_TODO = 'DEL_TODO'


export function loadTodoAction() {

  return (dispatch) => {
    axios.get('https://react-native-todo-8dc67-default-rtdb.europe-west1.firebasedatabase.app/todos.json')
      .then((res) => {
        const data = res.data;
        const todos = Object.keys(data).map(key => ({...data[key], id: key}));
        dispatch({type: LOAD_TODO, payload: todos})
      })
  }
}

export function addTodoAction(title) {

  return (dispatch) => {

    axios.post('https://react-native-todo-8dc67-default-rtdb.europe-west1.firebasedatabase.app/todos.json', {title})
      .then(res => dispatch({type: ADD_TODO, payload: {id: res.data.name, title}})
      )
  }
}

export function delTodoAction(id) {

  return (dispatch) => {
    axios.delete(`https://react-native-todo-8dc67-default-rtdb.europe-west1.firebasedatabase.app/todos/${id}.json`)
      .then(res => null)

    dispatch({type: DEL_TODO, payload: id})

  }
}

