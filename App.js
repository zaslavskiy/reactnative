import React from 'react';
import {StyleSheet, View} from 'react-native';

import {Provider} from 'react-redux';

import store from './src/store/index'

import {Navbar} from "./src/components/Navbar";
import {AddToDo} from "./src/components/AddToDo";
import {ToDoList} from "./src/components/ToDoList";


export default function App() {


  return (
    <Provider store={store}>
      <View style={styles.container}>
        <Navbar title={'Список Дел'}/>
        <AddToDo />
        <ToDoList />
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {},
  text: {
    // color: 'black'
  }
});
